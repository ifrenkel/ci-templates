# When using dind, it's wise to use the overlayfs driver for
# improved performance.
variables:
  # The context of these variables is global to the downstream job
  DOCKER_DRIVER: overlay2
  MAJOR: 1
  TMP_IMAGE: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA
  GO_VERSION: 1.15.0

stages:
  - maintenance      # update the project dependencies; only if MAINTENANCE is set
  - pre-build        # lint code, run unit tests, and compile binaries
  - build-package    # build distro package(s) for the analyzer and/or its dependencies
  - build-image      # build the Docker image(s) for the analyzer
  - test             # check, test, and scan the Docker images
  - performance-metrics # run performance checks
  - release-version  # release Docker images and distro packages
  - release-major    # update Docker images and distro packages of the major release
  - post             # run benchmarks

.if-gitlab-org-branch: &if-gitlab-org-branch
  if: '$CI_PROJECT_NAMESPACE != "gitlab-org/security-products/analyzers"'

# Only run for gitlab-org branches, as downstream needs exec permissions
.downstream_rules: &downstream_rules
  - if: $DOWNSTREAM_DISABLED
    when: never
  - <<: *if-gitlab-org-branch
  - when: never

.maintenance:
  stage: maintenance
  rules:
    - if: $MAINTENANCE
      when: always
    - when: never

maintenance_blocker:
  extends: .maintenance
  allow_failure: false
  script: echo "Dummy job to block the rest of the pipeline" && false

update_common:
  extends: .maintenance
  image: golang:$GO_VERSION
  variables:
    TITLE: "Upgrade to common ${COMMON_COMMIT_TAG}"
    TARGET_BRANCH: "$CI_COMMIT_REF_SLUG"
    SOURCE_BRANCH: "upgrade-to-common-$COMMON_COMMIT_TAG-$CI_JOB_ID"
  script: |
     set -x
     git config --global user.email "gitlab-bot@gitlab.com"
     git config --global user.name "GitLab Bot"
     git checkout -b $SOURCE_BRANCH
     go get gitlab.com/gitlab-org/security-products/analyzers/common/v2@${COMMON_COMMIT_TAG}
     go mod tidy
     git diff --quiet go.mod && exit 1
     git commit -m "$TITLE" go.sum go.mod
     git push -o merge_request.create -o merge_request.remove_source_branch -o merge_request.title="$TITLE" -o merge_request.label="backend" -o merge_request.label="devops::secure" -o merge_request.target="$TARGET_BRANCH" ${CI_PROJECT_URL/https:\/\/gitlab.com/https://gitlab-bot:$GITLAB_TOKEN@gitlab.com}.git $SOURCE_BRANCH

.go:
  image: golang:$GO_VERSION
  stage: go

go test:
  stage: pre-build
  extends: .go
  script:
    - go get ./...
    - go test -race -cover -v ./...
  coverage: '/coverage: \d+.\d+% of statements/'

go lint:
  extends: .go
  stage: pre-build
  script:
    - go get -u golang.org/x/lint/golint
    - golint -set_exit_status ./...

go mod tidy:
  extends: .go
  stage: pre-build
  script:
    - go mod tidy
    - git diff --exit-code go.mod go.sum

build tmp image:
  image: docker:stable
  stage: build-image
  services:
    - docker:19.03.5-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg GO_VERSION -t $TMP_IMAGE .
    - docker push $TMP_IMAGE

.docker_tag:
  image: docker:stable
  stage: release-version
  services:
    - docker:19.03.5-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export SOURCE_IMAGE=$TMP_IMAGE
    - export TARGET_IMAGE=$CI_REGISTRY_IMAGE:${IMAGE_TAG:-$CI_JOB_NAME}
    - docker pull $SOURCE_IMAGE
    - docker tag $SOURCE_IMAGE $TARGET_IMAGE
    - docker push $TARGET_IMAGE

.qa-downstream-ds:
  rules: *downstream_rules
  stage: test
  variables:
    SAST_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    SAST_DEFAULT_ANALYZERS: ""
    DS_DEFAULT_ANALYZERS: ""
    DS_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    DS_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SECURE_LOG_LEVEL: debug
  trigger:
    project: ""
    branch: master
    strategy: depend

.qa-downstream-sast:
  rules: *downstream_rules
  stage: test
  variables:
    DEPENDENCY_SCANNING_DISABLED: "true"
    CONTAINER_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    DS_DEFAULT_ANALYZERS: ""
    SAST_DEFAULT_ANALYZERS: ""
    SAST_ANALYZER_IMAGES: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SAST_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
    SAST_DISABLE_DIND: "false"
    SECURE_LOG_LEVEL: debug
  trigger:
    project: ""
    branch: master
    strategy: depend

.qa-downstream-cs:
  rules: *downstream_rules
  stage: test
  variables:
    SAST_DISABLED: "true"
    DEPENDENCY_SCANNING_DISABLED: "true"
    DAST_DISABLED: "true"
    LICENSE_MANAGEMENT_DISABLED: "true"
    SECURE_LOG_LEVEL: debug
    CS_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"
  trigger:
    project: ""
    branch: master
    strategy: depend

tag branch:
  extends: .docker_tag
  variables:
    # CAUTION: by preferring `SLUG` over `NAME` we can properly handle non-alphanumeric
    # characters, but this may limit our tags to 63chars or raise potential conflicts.
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
      when: never
    - if: $CI_COMMIT_BRANCH

tag edge:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME

tag version:
  extends: .docker_tag
  before_script:
    - export IMAGE_TAG=${CI_COMMIT_TAG/v/}
    - echo "Checking that $CI_COMMIT_TAG is last in the changelog"
    - test "$(grep '^## v' CHANGELOG.md |head -n 1)" = "## $CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_TAG

.release:
  extends: .docker_tag
  stage: release-major
  rules:
    - if: $CI_COMMIT_TAG

major:
  extends: .release
  variables:
    IMAGE_TAG: $MAJOR

latest:
  extends: .release

include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
  - template: SAST.gitlab-ci.yml
  - template: Container-Scanning.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    CI_APPLICATION_REPOSITORY: $CI_REGISTRY_IMAGE/tmp

benchmark:
  stage: post
  trigger: gitlab-org/security-products/sast-benchmark
  rules:
    # - Only run for SAST jobs on tagged releases
    # - Only run for gitlab-org MRs, as downstream needs exec permissions
    - if: '$REPORT_FILENAME == "gl-sast-report.json" && $CI_COMMIT_TAG && $CI_PROJECT_NAMESPACE == "gitlab-org/security-products/analyzers"'
    - when: never
